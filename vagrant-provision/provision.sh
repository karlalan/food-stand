#!/bin/sh

# PHP
sudo LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php
sudo apt update
sudo apt install -y php7.2 php7.2-common php7.2-cli php7.2-fpm php7.2-mysql php7.2-mbstring php7.2-xml

# MySQL(Maria-DB)
# This apt-key doesn't require sudo, but otherwise 'systemctl start mysqld' won't work.
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
sudo add-apt-repository -y 'deb [arch=amd64,arm64,i386,ppc64el] http://ftp.bme.hu/pub/mirrors/mariadb/repo/10.4/ubuntu xenial main'
sudo apt update
sudo apt install -y mariadb-server
sudo systemctl start mysqld
sudo mysqladmin -u root password 'password'
sudo mysql -u root -ppassword << __EOF__
SET character_set_database=utf8;
SET character_set_server=utf8;
GRANT ALL PRIVILEGES ON *.* TO root@'192.168.33.1' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON foodstand.* TO username@'127.0.0.1' IDENTIFIED BY 'password';
__EOF__

# Composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer
cd /vagrant
sudo apt install -y zip unzip
composer install

# Doctrine
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate --no-interaction
