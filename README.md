# food-stand
```
This is a WEB APP built by Angular 8 and Symfony 4. The app is SPA with Symfony 4 as RESTful API.
About installations and other details will be updated soon.
```

## System
```
[API Server]
Symfony 4.3

[WEB SPA]
Angular 8

[Database]
MySQL Server 5.7.25
※ Because the server is installed locally without any virtual env, it needs to be downloaded and installed first. Then create a database named foodstand. Its tables can be created by using doctrine commands:
step 1: $ cd  ~/food-stand/app
step 2: $ php bin/console make:migrations  ---> to create migration_versions table
step 3: $ php bin/console doctrine:migrations:execute --up 20191026140530 ---> to create required tables based on the files under ~/app/src/Migrations

※The user info about database can be set up at .env file.
```

## How to start server with Vagrant

```bash
cd /path/to/food-stand-api
vagrant up
vagrant ssh
cd /vagrant
php bin/console server:run 0.0.0.0:8000
```
