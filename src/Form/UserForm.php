<?php

namespace App\Form;

use Symfony\Form\AbstractType;
use Symfony\Form\FormBuilderInterface;
use Symfony\Form\Extension\Core\Type\SubmitTyp;
use Symfony\OptionsResolver\OptionsResolver;
use App\Entity\User;

class UserType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('name')
				->add('pass')
				->add('email')
				->add('authority');
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => User::class,
			'csrf_protection' => false
		));
	}
}